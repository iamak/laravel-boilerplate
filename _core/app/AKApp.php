<?php

namespace AK;

use Illuminate\Foundation\Application;

class AKApp extends Application
{
	/**
     * Get the path to the public / web directory.
     *
     * @return string
     */
    public function publicPath()  
    {
        //return '/';
        return $this->basePath.DIRECTORY_SEPARATOR.'../public_html';
    }

    /**
     * Get the path to the language files.
     *
     * @return string
     */
    public function langPath()
    {
        return $this->basePath.DIRECTORY_SEPARATOR.'../_resources'.DIRECTORY_SEPARATOR.'lang';
    }
}
