<?php

namespace AK\Models\Web;

use AK\Models\Web\WebModel;
use Hash;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class WebUser extends Authenticatable
{
    use SoftDeletes;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'web_users';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The model's attributes.
     *
     * @var array
     */
    protected $attributes = [
        'usergroup_id' => 1,
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['password','remember_token'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['usergroup_id','email','password','fullname','firstname','lastname','phoneno','gender','dob','mobileno','city_name','alternate_no','blood_group_id','country_id','state_id','district_id','city_id','zipcode','address_line_1','address_line_2','email_verified','phoneno_verified','verification_code','verification_hash','active','verified','published','available','send_notifications','blocked','deleted','subscribe_newsletter','dob_date','ipaddress','settings','social_login_info','availability'];

    /**
     * Set the password hash
     *
     * @param  string  $value
     * @return string
     */
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    public function scopePublished($query)
    {
        return $query->where('active',1)->where('published',1);
    }

    public function scopeSearch($query,$keyword, $onlyzip=false)
    {
        if($onlyzip == true){
            return $query->where('zipcode','like','%'.$keyword.'%');
        }

        return $query->where(function ($query) use ($keyword){
                    $query->orWhere('zipcode','like','%'.$keyword.'%')
                     ->orWhere('fullname','like','%'.$keyword.'%')
                     ->orWhere('email','like','%'.$keyword.'%')
                     ->orWhere('mobileno','like','%'.$keyword.'%')
                     ->orWhere('mobileno','like','%'.$keyword.'%')
                     ->orWhere('phoneno','like','%'.$keyword.'%')
                     ->orWhere('alternate_no','like','%'.$keyword.'%');
        });
        
    }

    public function bloodgroup()
    {
        return $this->belongsTo('AK\Models\Web\BloodGroup','blood_group_id');
    }

}
