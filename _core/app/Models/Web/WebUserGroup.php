<?php

namespace AK\Models\Web;

use AK\Models\Web\WebModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class WebUserGroup extends WebModel
{
	use SoftDeletes;
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
    protected $table = 'web_user_groups';

    /**
     * The model's attributes.
     *
     * @var array
     */
    protected $attributes = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['short_description','description','previlages','page_access','active','verified','published','blocked','deleted','created_by','created_ip','updated_ip'];

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','name','short_description','description','previlages','page_access','active','verified','published','blocked','deleted','created_by','updated_by', 'created_ip','updated_ip'];

}