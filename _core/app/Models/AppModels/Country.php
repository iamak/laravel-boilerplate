<?php

namespace AK\Models\AppModels;

use AK\Models\AppModels\AppModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Country extends AppModel
{
    use SoftDeletes;
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
    protected $table = 'countries';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The model's attributes.
     *
     * @var array
     */
    protected $attributes = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['id','iso','name','longname','iso3','numcode','countrycode','active','verified','published','blocked','deleted','created_by','created_ip','updated_by','updated_ip'];

}