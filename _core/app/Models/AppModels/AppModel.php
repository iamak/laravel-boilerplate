<?php

namespace AK\Models\AppModels;

use Illuminate\Database\Eloquent\Model;

class AppModel extends Model
{
    public function scopeActive($query)
    {
    	return $query->where('active',1);
    }

    public function scopePublished($query)
    {
    	return $query->active()->where('published',1);
    }
}
