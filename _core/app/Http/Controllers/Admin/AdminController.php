<?php

namespace AK\Http\Controllers\Admin;

use Illuminate\Http\Request;

use AK\Http\Requests;
use AK\Http\Controllers\AKApp\AKAppController;

class AdminController extends AKAppController
{
    protected $redirectTo = '/app';
	protected $guard = 'admin';
}