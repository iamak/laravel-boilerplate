<?php

namespace AK\Http\Controllers\Admin;

use Illuminate\Http\Request;

use AK\Http\Requests;
use AK\Http\Controllers\Admin\AdminController;

class DashboardController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.templates.dashboard');
    }
}
