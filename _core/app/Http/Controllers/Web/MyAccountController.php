<?php

namespace AK\Http\Controllers\Web;

use Illuminate\Http\Request;

use AK\Http\Requests;
use AK\Http\Controllers\Web\WebController;

use AK\Models\Web\BloodGroup;
use AK\Models\Web\WebUser;

use Validator;

use Auth;

class MyAccountController extends WebController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$bloodGroups = BloodGroup::published()->orderBy('name','ASC')->pluck('name', 'id');
        $user = Auth::user();
        return view('templates.myaccount', compact('bloodGroups','user'));
    }

    public function postUpdate(Request $request)
    {
        $input = $request->all();

        $user = Auth::user();

        $validationRules = [
            'fullname' => 'required|min:3',
            'blood_group_id' => 'required',
            'zipcode' => 'required|numeric|min:5',
            'city_name' => 'required|min:3',
            'mobileno' => 'required|numeric|min:10',
            'phoneno' => 'required|numeric|min:9',
            'gender' => 'required',
            'dob' => 'required|date',
            'email' => 'required|unique:web_users,email,'.$user->id.'|email',
        ];

        if($input['password']){
            $validationRules['password'] = 'required|min:8|confirmed|max:25';
        }

        $validator = Validator::make($input, $validationRules)->validate();

        $user->update($input);
        
        return redirect()->back()->with('user');
    }
}
