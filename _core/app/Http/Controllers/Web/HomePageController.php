<?php

namespace AK\Http\Controllers\Web;

use Illuminate\Http\Request;

use AK\Http\Requests;
use AK\Http\Controllers\Web\WebController;

use AK\Models\Web\BloodGroup;
use AK\Models\Web\WebUser;

use Validator;

class HomePageController extends WebController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$bloodGroups = BloodGroup::published()->orderBy('name','ASC')->pluck('name', 'id');
        return view('templates.homepage', compact('bloodGroups'));
    }

    public function search()
    {
        $bloodGroups = BloodGroup::published()->orderBy('name','ASC')->pluck('name', 'id');
        $webusers = WebUser::published()->orderBy('id','DESC')->paginate(20);
        return view('templates.search', compact('bloodGroups','webusers'));
    }
}
