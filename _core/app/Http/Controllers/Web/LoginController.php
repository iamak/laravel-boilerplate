<?php

namespace AK\Http\Controllers\Web;

use Illuminate\Http\Request;

use AK\Http\Requests;
use AK\Http\Controllers\Web\WebController;

use AK\Models\Web\BloodGroup;
use AK\Models\Web\WebUser;

use Validator;
use Auth;
use Illuminate\Support\MessageBag;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
class LoginController extends WebController
{
    use AuthenticatesUsers;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	//$bloodGroups = BloodGroup::published()->orderBy('name','ASC')->pluck('name', 'id');
        return view('templates.login');
    }

    public function postLogin(Request $request)
    {
        $input = $request->only('email','password');

        $validationRules = [
            'email' => 'required|email',
            'password' => 'required',
        ];

        $validator = Validator::make($input, $validationRules)->validate();        
        
        if (Auth::attempt(['email' => $input['email'], 'password' => $input['password'], 'active' => 1, 'blocked' => 0])) {
            // Authentication passed...
            return redirect()->intended('myaccount');
        }
        else{
            $errors = new MessageBag(['password' => ['Email and/or password is invalid. Please try again.']]);
            return redirect()->back()->withErrors($errors)->withInput($request->except('password'));
        }
    }

    public function logout()
    {
        Auth::logout();
         return redirect()->intended('/');
    }
}
