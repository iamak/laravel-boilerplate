<?php

namespace AK\Http\Controllers\Web;

use Illuminate\Http\Request;

use AK\Http\Requests;
use AK\Http\Controllers\Web\WebController;

use AK\Models\Web\BloodGroup;
use AK\Models\Web\WebUser;

use Validator;

use Session;

class SearchPageController extends WebController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword = $request->session()->get('keyword', null);
        $blood_group_id = $request->session()->get('blood_group_id', null);
        $onlyzip = $request->session()->get('onlyzip', false);

    	$bloodGroups = BloodGroup::published()->orderBy('name','ASC')->pluck('name', 'id');

        $webusers = null;

        if($keyword && $blood_group_id){
            
            $webusers = WebUser::published()->with('bloodgroup')
                        ->where('blood_group_id',$blood_group_id)
                        ->search($keyword, $onlyzip)
                        ->orderBy('id','DESC')
                        ->paginate(20);
            return view('templates.search', compact('bloodGroups', 'webusers', 'keyword', 'blood_group_id'));
        }

        return view('templates.search', compact('bloodGroups', 'webusers', 'keyword', 'blood_group_id'));
    }

    public function search(Request $request)
    {
        $input = $request->only('keyword','blood_group_id','onlyzip');
        
        $request->session()->put('keyword', $input['keyword']);
        $request->session()->put('blood_group_id', $input['blood_group_id']);        

        $bloodGroups = BloodGroup::published()->orderBy('name','ASC')->pluck('name', 'id');

        $keyword = $input['keyword'];
        $blood_group_id = $input['blood_group_id'];
        
        $onlyzip = false;

        if(isset($input['onlyzip'])){
            $onlyzip = $input['onlyzip'];
            $request->session()->put('onlyzip', $input['onlyzip']);
            $onlyzip = true;
        }
        else{
            $request->session()->put('onlyzip', false);
        }
        
        $webusers = WebUser::published()->with('bloodgroup')
                            ->where('blood_group_id',$input['blood_group_id'])
                            ->search($input['keyword'], $onlyzip)
                            ->orderBy('id','DESC')
                            ->paginate(20);

        return view('templates.search', compact('bloodGroups','webusers', 'keyword', 'blood_group_id', 'onlyzip'));
    }
}
