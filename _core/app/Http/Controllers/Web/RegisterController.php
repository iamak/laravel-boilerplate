<?php

namespace AK\Http\Controllers\Web;

use Illuminate\Http\Request;

use AK\Http\Requests;
use AK\Http\Controllers\Web\WebController;

use AK\Models\Web\BloodGroup;
use AK\Models\Web\WebUser;

use Validator;

use Auth;

use Illuminate\Foundation\Auth\AuthenticatesUsers;

class RegisterController extends WebController
{
    use AuthenticatesUsers;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$bloodGroups = BloodGroup::published()->orderBy('name','ASC')->pluck('name', 'id');
        return view('templates.register', compact('bloodGroups'));
    }

    public function postRegister(Request $request)
    {
        $input = $request->all();

        $input['email'] = $input['mobileno'];

        $password = $this->randomPassword();

        $validationRules = [
            'fullname' => 'required|min:3',
            'blood_group_id' => 'required',
            'zipcode' => 'required|numeric|min:5',
            'mobileno' => 'required|numeric|min:10|unique:web_users,mobileno',
            'phoneno' => 'numeric|min:9',
            'email' => 'required|unique:web_users,email',
            // 'password' => 'required|min:8|confirmed|max:25',
            //'password_confirmation' => 'required|confirmed',
        ];

        $validator = Validator::make($input, $validationRules)->validate();

        $input['password'] = $password;
        $input['settings'] = $password;
        
        $input['email'] = $input['mobileno'];
        $input['active']= 1;
        $input['published']= 1;

        $User = WebUser::create($input);

        if($User){
            if (Auth::attempt(['email' => $input['email'], 'password' => $input['password']])) {
                // Authentication passed...
                return redirect()->intended('myaccount');
            }
        }

        //return $User;
    }
}
