<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
// Display all SQL executed in Eloquent
// \Event::listen('Illuminate\Database\Events\QueryExecuted', function ($query) {
//     var_dump($query->sql);
//     var_dump($query->bindings);
//     var_dump($query->time);
// });

Route::group(['prefix' => '', 'namespace'=>'Web', 'as' => 'web:'], function () {

	Route::get('/', 'HomePageController@index')->name('homepage');
	Route::get('/search', 'SearchPageController@index')->name('search');
	Route::post('/search', 'SearchPageController@search')->name('postSearch');
	Route::get('/register', 'RegisterController@index')->name('getRegister');
	Route::post('/register', 'RegisterController@postRegister')->name('postRegister');
	Route::get('/login', 'LoginController@index')->name('getLogin');
	Route::post('/login', 'LoginController@postLogin')->name('postLogin');
	
	Route::group(['prefix' => 'myaccount', 'as' => 'myaccount', 'middleware' => 'auth'], function () {
		Route::get('/', 'MyAccountController@index')->name('myaccount');
		Route::post('/', 'MyAccountController@postUpdate')->name('myaccountUpdate');
	});

	Route::any('/logout', 'LoginController@logout')->name('logout');
});

Route::group(['prefix' => 'app', 'namespace'=>'Admin', 'as' => 'admin:'], function () {
    Route::resource('/', 'DashboardController', ['only' => ['index'], 'names' => [
    	'index' => 'dashboard'
	]]);
});
