<?php

use Illuminate\Database\Seeder;
use AK\Models\AppModels\Country;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
                'id' => 1, 
                'iso'    => 'IN', 
                'name'   => 'INDIA', 
                'longname'   => 'India', 
                'iso3'   => 'IND', 
                'numcode' => 356, 
                'countrycode' => 91,
                'active' => 1,
                'published' => 1,
                'verified' => 1,
            ];        
        Country::create($data);
    }
}
