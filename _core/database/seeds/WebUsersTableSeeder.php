<?php

use Illuminate\Database\Seeder;

use AK\Models\Web\WebUser;

use Faker\Generator;

class WebUsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$faker = Faker\Factory::create();

    	for($i=1;$i<1000;$i++){    		
    		$data = [
	    		'email' => $faker->unique()->safeEmail,
				'password' => '123456789',
				'remember_token' => str_random(10),
				'fullname'  => $faker->name,
				'firstname'  => $faker->firstname,
				'lastname'  => $faker->lastname,
				'phoneno'  => $faker->e164PhoneNumber,
				'gender' => 'male',
				'dob'  => $faker->date('Y-m-d'),
				'mobileno' => $faker->e164PhoneNumber,
				'city_name' => $faker->city,
				'alternate_no' => $faker->e164PhoneNumber,
				'blood_group_id' => mt_rand(1,18),
				'zipcode' => mt_rand(111111,999999),
				'availability'=>'available',
				'active' => 1,
				'verified' => 1,
				'published' => 1,
				'available' => 1,
				'send_notifications' => 1,
				'subscribe_newsletter' => 1,
				'dob_date'  => $faker->date('Y-m-d')
			];
			WebUser::create($data);
    	}
        //factory(WebUser::class, 10000)->create();
    }
}
