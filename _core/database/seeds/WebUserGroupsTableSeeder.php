<?php

use Illuminate\Database\Seeder;

use AK\Models\Web\WebUserGroup;

class WebUserGroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
        'name' => 'Customer',
        'short_description' => 'User Groups for customers',        
        'active' => 1,
        'verified' => 1,
        'published' => 1
        ];
        WebUserGroup::create($data);
    }
}
