<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cities', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('country_id')->unsigned();
            $table->foreign('country_id')->references('id')->on('countries');
            $table->integer('state_id')->unsigned();
            $table->foreign('state_id')->references('id')->on('states');
            $table->integer('district_id')->unsigned();
            $table->foreign('district_id')->references('id')->on('districts');
            $table->integer('taluk_id')->unsigned();
            $table->foreign('taluk_id')->references('id')->on('taluks');
            $table->string('name');
            $table->string('longname');
            $table->boolean('active')->default(0);
            $table->boolean('verified')->default(0);
            $table->boolean('published')->default(0);
            $table->boolean('blocked')->default(0);
            $table->boolean('deleted')->default(0);
            $table->string('created_by')->default('system');
            $table->string('created_ip')->default('localhost');
            $table->string('updated_by')->default('system');
            $table->string('updated_ip')->default('localhost');
            $table->softDeletes();            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cities');
    }
}
