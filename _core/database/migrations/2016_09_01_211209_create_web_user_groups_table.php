<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWebUserGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('web_user_groups', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('short_description');
            $table->mediumText('description');
            $table->longText('previlages')->nullable();
            $table->longText('page_access')->nullable();
            $table->boolean('active')->default(0);
            $table->boolean('verified')->default(0);
            $table->boolean('published')->default(0);
            $table->boolean('blocked')->default(0);
            $table->boolean('deleted')->default(0);
            $table->string('created_by')->default('system');
            $table->string('created_ip')->default('localhost');
            $table->string('updated_by')->default('system');
            $table->string('updated_ip')->default('localhost');
            $table->softDeletes();            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('web_user_groups');
    }
}
