<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('iso');
            $table->string('name');
            $table->string('longname');
            $table->string('iso3');
            $table->string('numcode');
            $table->string('countrycode');
            $table->boolean('active')->default(0);
            $table->boolean('verified')->default(0);
            $table->boolean('published')->default(0);
            $table->boolean('blocked')->default(0);
            $table->boolean('deleted')->default(0);
            $table->string('created_by')->default('system');
            $table->string('created_ip')->default('localhost');
            $table->string('updated_by')->default('system');
            $table->string('updated_ip')->default('localhost');
            $table->softDeletes();            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('countries');
    }
}
