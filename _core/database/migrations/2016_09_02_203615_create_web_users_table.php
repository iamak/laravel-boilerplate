<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWebUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('web_users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('usergroup_id')->unsigned();
            $table->foreign('usergroup_id')->references('id')->on('web_user_groups');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('fullname');
            $table->string('firstname');
            $table->string('lastname');
            $table->string('phoneno');
            $table->string('gender');
            $table->string('dob');
            $table->string('availability');
            $table->string('mobileno')->unique();
            $table->string('city_name')->nullable();
            $table->string('alternate_no')->nullable();
            $table->integer('blood_group_id')->unsigned()->nullable();
            $table->integer('country_id')->unsigned()->nullable();
            $table->integer('state_id')->unsigned()->nullable();
            $table->integer('district_id')->unsigned()->nullable();
            $table->integer('city_id')->unsigned()->nullable();
            $table->integer('zipcode')->unsigned()->nullable();
            $table->longText('address_line_1')->nullable();
            $table->longText('address_line_2')->nullable();            
            $table->boolean('email_verified');
            $table->boolean('phoneno_verified');
            $table->string('verification_code');
            $table->string('verification_hash');
            $table->boolean('active');
            $table->boolean('verified');
            $table->boolean('published');
            $table->boolean('available');
            $table->boolean('send_notifications');
            $table->boolean('blocked');
            $table->boolean('deleted');
            $table->boolean('subscribe_newsletter');
            $table->date('dob_date');
            $table->ipAddress('ipaddress');
            $table->longText('settings');
            $table->longText('social_login_info');
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('web_users');
    }
}
