<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define((AK\Models\Web\WebUser::class, function (Faker\Generator $faker) {    
    return [
        'email' => $faker->unique()->safeEmail,
		'password' => '123456789',
		'remember_token' => str_random(10),
		'fullname'  => $faker->name,
		'firstname'  => $faker->firstname,
		'lastname'  => $faker->lastname,
		'phoneno'  => $faker->e164PhoneNumber,
		'gender' => 'male',
		'dob'  => $faker->date('Y-m-d'),
		'mobileno' => $faker->e164PhoneNumber,
		'city_name' => $faker->cityName,
		'alternate_no' => $faker->e164PhoneNumber,
		'blood_group_id' => mt_rand(1,18),
		'zipcode' => mt_rand(111111,999999),
		'active' => 1,
		'verified' => 1,
		'published' => 1,
		'available' => 1,
		'send_notifications' => 1,
		'subscribe_newsletter' => 1,
		'dob_date'  => $faker->date('Y-m-d'),		
    ];
});
